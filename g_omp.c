#include <stdio.h>
#include <stdlib.h>

int rows, columns;

/*prints the matrix, used for debugging*/
void printMatrix(char** mat){

	int i, j;

	for(i = 0; i < rows; i++){
		for(j = 0; j < columns; j++){
			printf("%c", mat[i][j]);
		}
		printf("\n");
	}
}

/*counts the number of alive neighbors of a cell*/
int countAlive(int i, int j, char** mat){

	int ip, jp, alive = 0;

	for(ip = i - 1; ip < i + 2; ip++)
		for(jp = j - 1; jp < j + 2; jp++){
			if(mat[ip][jp] == 'X')
				alive++;
		}
	if(mat[i][j] == 'X')
		alive--;
	return alive;
}

/*updates the matrix after one iteration*/
void copyMatrices( char** mat, char** newMat){

	int i, j;

	#pragma omp parallel for private(j) collapse(2)
	for(i = 0; i < rows; i++)
		for(j = 0; j < columns; j++){
			mat[i][j] = newMat[i][j];
		}
}

/*performs the checking to decide the state of the cell within the itertion*/
void step(char** mat, char** newMat){

	int i, j;

	#pragma omp parallel for private(j) collapse(2)
	for(i = 1; i < rows - 1; i++)
		for(j = 1; j < columns - 1; j++){
			int alive = countAlive(i, j, mat);

			if(alive > 3){
				newMat[i][j] = '.';

			}

			if(alive < 2){
				newMat[i][j] = '.';

			}

			if(mat[i][j] == 'X')									
				if(alive == 2 || alive == 3){
					newMat[i][j] = 'X';

				}

			if(mat[i][j] == '.')
				if(alive == 3){
					newMat[i][j] = 'X';

				}
		}
}

/*writes the matrix in the output file*/
void writeOutput(FILE *fileout, char** mat){

	int i, j;

	for(i = 1; i < rows - 1; i++) {
		for(j = 1; j < columns - 1; j++) {
		    fprintf(fileout,"%c ",mat[i][j]);
		}
		fprintf(fileout,"\n");
	}
}

/*reads the matrix from the input file*/
void readInput(FILE *filein, char** mat){
	int i, j;
	for(i = 1; i < rows - 1; i++)
	{
	    for(j = 1; j < columns - 1; j++) 
	    {
		    fscanf(filein, "%c", &mat[i][j]);  
		    fgetc(filein);
	    }
	    fgetc(filein);// take the enter at the end of row
	}
}

/*builds the borders of the matrix*/
void makeBorders(char** mat){

	int i;

	//border first and last row
	#pragma omp parallel for
	for(i = 1; i < columns - 1; i++){
		mat[0][i] = mat[rows - 2][i];
		mat[rows - 1][i] = mat[1][i];
	}

	//border first and last column
	#pragma omp parallel for
	for(i = 1; i < rows - 1; i++){
		mat[i][0] = mat[i][columns - 2];
		mat[i][columns - 1] = mat[i][1];
	}

	//border corners
	mat[0][0] = mat[rows - 2][columns - 2];
	mat[rows - 1][0] = mat[1][columns - 2];
	mat[0][columns - 1] = mat[rows - 2][1];
	mat[rows - 1][columns - 1] = mat[1][1];

}
int main(int argc, char *argv[]){

	int i, numIterations;
	FILE *filein, *fileout;

	if(argc != 4){
		printf("Incorrect number of arguments\n");
		return 0;
	}
	
	/*open input and output files*/
  	filein = fopen(argv[1], "r");
  	fileout = fopen(argv[3], "w");

  	numIterations = atoi(argv[2]);
  	fscanf(filein, "%d %d\n", &rows, &columns);

  	rows += 2;//add border rows
  	columns += 2;//add border columns

  	/*dynamic alocation for the matrices*/
  	char** mat = malloc(rows * sizeof(char*)); 
  	char** newMat = malloc(rows * sizeof(char*));
	for(i = 0; i < rows; i++){
		mat[i] = malloc(columns * sizeof(char));
		newMat[i] = malloc(columns * sizeof(char));
	}

	readInput(filein, mat);
	makeBorders(mat);

	copyMatrices(newMat, mat);//initialize new matrix

	for(i = 0; i < numIterations; i++){		
		step(mat, newMat);
		copyMatrices(mat, newMat);
		makeBorders(mat);
		makeBorders(newMat);
	}
	
	writeOutput(fileout, mat);
	/*close input and output files*/
	fclose(filein);
	fclose(fileout);
}