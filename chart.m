
x = [1, 2, 4, 8];
ys = [61.103, 60.900, 61.066, 61.406];

yp = [62.116, 31.317, 15.849, 8.113];


plot(x, ys)
xlabel ('Number of threads');
ylabel ('Time (seconds)');
legend ('Serial run');
hold on
plot(x, yp, 'r')
legend ('Serial run','Parallel run');