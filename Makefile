.PHONY: all clean clean_all

CC = gcc
CFLAGS_S = -Wall
CFLAGS_P = -fopenmp -Wall

all: build

build: serial parallel

serial : g_serial.c
	$(CC) $^ -o g_serial $(CFLAGS_S)

parallel : g_omp.c
	$(CC) $^ -o g_omp $(CFLAGS_P)

clean :
	rm -rf g_serial
	rm -rf g_omp
	rm -rf out*.txt


